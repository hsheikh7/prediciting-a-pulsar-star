

# Getting and Checking Data

#import essential libraries

import pandas as nd #processing dataframes
import numpy as np #linear algebra
import matplotlib.pyplot as plt


# Getting data
df = pd.read_csv(r'C:\Users\Hassan\PycharmProjects\prediciting-a-pulsar-star\pulsar_stars.csv')
df

## Exploring Data

df.head(6) #the very first rows of dataframe

df.info() #Information about data types and amount of non-null rows of our Datas

df.describe() #statistical information about the data

df.corr()    # correlation between fields

########Tahlil ina yadam nare - Tafsir

## Visual EDA

import matplotlib.pyplot as plt    # basic plotting library
import seaborn as sns              # more advanced visual plotting library


sns.pairplot(data=df,palette="husl",hue="target_class",
             vars=["Mean_of_the_integrated_profile",
                   "Excess_kurtosis_of_the_integrated_profile",
                   "Skewness_of_the_integrated_profile",
                   "Mean_of_the_DM_SNR_curve",
                   " Excess_kurtosis_of_the_DM_SNR_curve",
                   " Skewness_of_the_DM_SNR_curve"])

plt.suptitle("PairPlot of Data Without Std. Dev. Fields",fontsize=18)

plt.tight_layout()
plt.show()   # pairplot without standard deviaton fields of data

plt.figure(figsize=(16,12))
sns.heatmap(data=df.corr(),annot=True,cmap="bone",linewidths=1,fmt=".2f",linecolor="gray")
plt.title("Correlation Map",fontsize=20)
plt.tight_layout()
plt.show()      # lightest and darkest cells are most correlated ones

plt.figure(figsize=(16,10))

plt.subplot(2,2,1)
sns.violinplot(data=df,y="Mean_of_the_integrated_profile",x="target_class")

plt.subplot(2,2,2)
sns.violinplot(data=df,y="Mean_of_the_DM_SNR_curve",x="target_class")

plt.subplot(2,2,3)
sns.violinplot(data=df,y="Standard_deviation_of_the_integrated_profile",x="target_class")

plt.subplot(2,2,4)
sns.violinplot(data=df,y="Standard_deviation_of_the_DM_SNR_curve",x="target_class")


plt.suptitle("ViolinPlot",fontsize=20)

plt.show()


# Data PreProcessing

## Splitting the Feature and Label fields

labels = df.target_class.values

df.drop(["target_class"],axis=1,inplace=True)

features = df.values

## Scaling the Features

from sklearn.preprocessing import MinMaxScaler
scaler = MinMaxScaler(feature_range=(0,1))

features_scaled = scaler.fit_transform(features)

## Splitting the Train and the Test rows

from sklearn.model_selection import train_test_split

x_train, x_test, y_train, y_test = train_test_split(features_scaled,labels,test_size=0.2)

# Modelling

## Logistic Regression

from sklearn.linear_model import LogisticRegression
lr_model = LogisticRegression(random_state=42,solver="liblinear",C=1.6,penalty="l1")

lr_model.fit(x_train,y_train)

y_head_lr = lr_model.predict(x_test)

lr_score = lr_model.score(x_test,y_test)

## Decision Tree Classifier

from sklearn.tree import DecisionTreeClassifier
dc_model = DecisionTreeClassifier(random_state=42)

dc_model.fit(x_train,y_train)

y_head_dc = dc_model.predict(x_test)

dc_score = dc_model.score(x_test,y_test)

## Random Forest Classifier

from sklearn.ensemble import RandomForestClassifier
rfc_model = RandomForestClassifier(n_estimators=37,random_state=42,max_leaf_nodes=200,criterion="entropy")

rfc_model.fit(x_train,y_train)

y_head_rfc = rfc_model.predict(x_test)

rfc_score = rfc_model.score(x_test,y_test)

## Naive Bayes Classifier

from sklearn.naive_bayes import GaussianNB
nb_model = GaussianNB()

nb_model.fit(x_train,y_train)

y_head_nb = nb_model.predict(x_test)

nb_score = nb_model.score(x_test,y_test)

## KNN: K Nearest Neighbors

from sklearn.neighbors import KNeighborsClassifier
knn_model = KNeighborsClassifier(n_neighbors=7,weights="distance")

knn_model.fit(x_train,y_train)

y_head_knn = knn_model.predict(x_test)

knn_score = knn_model.score(x_test,y_test)

## Support Vector Machine

from sklearn.svm import SVC
svm_model = SVC(random_state=42,C=250,gamma=1.6,kernel="poly",probability=True)

svm_model.fit(x_train,y_train)

y_head_svm = svm_model.predict(x_test)

svm_score = svm_model.score(x_test,y_test)

# Model Evaluating

## Confusion Matrix

from sklearn.metrics import confusion_matrix

cm_lr = confusion_matrix(y_test,y_head_lr)
cm_dc = confusion_matrix(y_test,y_head_dc)
cm_knn = confusion_matrix(y_test,y_head_knn)
cm_nb = confusion_matrix(y_test,y_head_nb)
cm_rfc = confusion_matrix(y_test,y_head_rfc)
cm_svm = confusion_matrix(y_test,y_head_svm)

plt.figure(figsize=(24,12))

plt.suptitle("Confusion Matrixes",fontsize=24)

plt.subplot(2,3,1)
plt.title("Logistic Regression Confusion Matrix")
sns.heatmap(cm_lr,cbar=False,annot=True,cmap="CMRmap_r",fmt="d")

plt.subplot(2,3,2)
plt.title("Decision Tree Classifier Confusion Matrix")
sns.heatmap(cm_dc,cbar=False,annot=True,cmap="CMRmap_r",fmt="d")

plt.subplot(2,3,3)
plt.title("K Nearest Neighbors Confusion Matrix")
sns.heatmap(cm_knn,cbar=False,annot=True,cmap="CMRmap_r",fmt="d")

plt.subplot(2,3,4)
plt.title("Naive Bayes Confusion Matrix")
sns.heatmap(cm_nb,cbar=False,annot=True,cmap="CMRmap_r",fmt="d")

plt.subplot(2,3,5)
plt.title("Random Forest Confusion Matrix")
sns.heatmap(cm_rfc,cbar=False,annot=True,cmap="CMRmap_r",fmt="d")

plt.subplot(2,3,6)
plt.title("Support Vector Machine Confusion Matrix")
sns.heatmap(cm_svm,cbar=False,annot=True,cmap="CMRmap_r",fmt="d")

plt.show()

## Bar Chart Comparison

algorithms = ("Logistic Regression","Decision Tree","Random Forest","K Nearest Neighbors","Naive Bayes","Support Vector Machine")
scores = (lr_score,dc_score,rfc_score,knn_score,nb_score,svm_score)
y_pos = np.arange(1,7)
colors = ("red","gray","purple","green","orange","blue")

plt.figure(figsize=(24,12))
plt.xticks(y_pos,algorithms,fontsize=18)
plt.yticks(np.arange(0.00, 1.01, step=0.01))
plt.ylim(0.90,1.00)
plt.bar(y_pos,scores,color=colors)
plt.grid()
plt.suptitle("Bar Chart Comparison of Models",fontsize=24)
plt.show()


